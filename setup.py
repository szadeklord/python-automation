#!/usr/bin/python
# -*- coding: utf-8 -*-

from setuptools import find_packages, setup


def get_version_info(key: str) -> str:
    author = ""
    with open("automation/_version.py", "r", encoding="utf-8") as f:
        for line in f:
            if line.startswith(key):
                author = eval(line.split("=")[-1])
                break
    return author


with open("README.md", "r", encoding="utf-8") as f:
    readme = f.read()

setup(
    name=get_version_info("__title__"),
    version=get_version_info("__version__"),
    description="A set of python utilities for automation of daily tasks",
    long_description=readme,
    long_description_content_type="text/markdown",
    author=get_version_info("__author__"),
    author_email=get_version_info("__email__"),
    license=get_version_info("__license__"),
    url="https://gitlab.com/szadeklord/python-automation",
    packages=find_packages(exclude=["docs*", "tests*"]),
    install_requires=["python-dateutil", "python-docx", "pywin32", "selenium-requests"],
    package_data={
        "automation": ["py.typed"],
    },
    python_requires=">=3.7.0",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: Microsoft :: Windows",
    ],
    extras_require={
        "autocompletion": ["argcomplete>=1.10.0,<3"],
    },
)
