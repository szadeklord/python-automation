import base64
from pathlib import Path
from typing import Dict

import win32com.client  # type: ignore

from automation import text_substitutor, utility
from automation.report_data import ReportData


class Email(text_substitutor.TextSubstitutor):
    def get_substitutions(self) -> Dict[str, str]:
        pass

    def send_email(self, report_data: ReportData) -> bool:
        # Get all relevant config values here for maintainability/readability
        config = report_data.config

        try:
            substitutions = config["substitutions"]
            dry_run = config["email"]["dry_run"]
            dry_run_email = config["email"]["dry_run_email"]
            email_template_path = Path(config["email"]["template_path"])
            email_template_project_path = Path(config["email"]["template_project_path"])
            email_title = config["email"]["title"]
            attachments = config["email"]["attachments"]
            inline_image_paths = config["email"]["inline_image_paths"]
            cc_addresses = config["email"]["cc"]
            bcc_addresses = config["email"]["bcc"]
        except Exception as e:
            print("One or more configuration values for email are invalid.")
            print(e)
            exit()

        # Turn substitutions from a regular dict into a failsafe dict so we can lazily replace without errors
        substitutions = utility.FailsafeDict(substitutions)

        # Add in inline image substitutions
        for image_path in inline_image_paths:
            image_path = Path(image_path)
            with open(image_path, "rb") as img_file:
                image_str = base64.b64encode(img_file.read()).decode()
            substitutions[image_path.stem] = image_str

        outlook = win32com.client.Dispatch("outlook.application")

        email = outlook.CreateItem(0)

        to_addresses = []
        project_names = []
        projects_body = ""

        with open(email_template_project_path, "r") as in_file:
            project_template_text = in_file.read()

        # Determine list of recipients and create html for each project
        # Projects use the following specific keywords:
        # REPORT_LINK
        # PROJECT_NAME
        # PROJECT_MEMBERS
        # LEAD_DUE_DATE
        # LEAD_MONTH
        for project in report_data.projects:
            to_addresses.extend(project.config["members"])
            project_names.append(project.config["name"])
            project.substitutions["REPORT_LINK"] = project.destination_url

            projects_body += project_template_text.format_map(project.substitutions)

        substitutions["PROJECTS"] = ", ".join(project_names)
        substitutions["PROJECTS_HTML"] = projects_body

        if dry_run:
            email.To = dry_run_email
        else:
            email.To = ";".join(set(to_addresses))
            email.CC = ";".join(set(cc_addresses))
            email.BCC = ";".join(set(bcc_addresses))

        email.Subject = email_title.format_map(substitutions)

        # Due to the nature of Python format strings python will try to replace the {} in the CSS so we have to read each line in individually and format on a per-line basis.
        with open(email_template_path, "r") as in_file:
            template_text_lines = in_file.readlines()

        for i, line in enumerate(template_text_lines):
            try:
                template_text_lines[i] = line.format_map(substitutions)
            except Exception as e:
                # Hacky. We have to avoid formatting the CSS within the HTML file.
                # Typical exceptions seen:
                # "expected '}' before end of string"
                # "Single '}' encountered in format string"
                if "'}'" not in str(e):
                    raise e

        email.HTMLBody = "".join(template_text_lines)

        # This will override the HTMLbody field if used.
        # email.Body = "This is a plaintext body"

        for file in attachments:
            full_path = str(Path(file).resolve())
            email.Attachments.Add(full_path)

        email.Send()
        return True
