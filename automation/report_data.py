from typing import Any, List

from automation.project_data import ProjectData


class ReportData(object):
    def __init__(self, config: Any, projects: List[ProjectData] = []):
        self.config = config
        self.projects = projects
