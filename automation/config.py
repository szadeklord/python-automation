import datetime
import json
from calendar import monthrange
from typing import Any, IO

from dateutil import relativedelta  # type: ignore

from automation.text_substitutor import TextSubstitutor


class Config(object):
    def __init__(self, substitutors: Any):
        self.data: Any = None
        self.text_substitutors = substitutors

        # Check to ensure all substitutors passed in are compliant
        for sub_class in self.text_substitutors:
            if not issubclass(sub_class, TextSubstitutor):
                raise TypeError(
                    "{} is not a sub-type of class: {}".format(
                        sub_class, TextSubstitutor
                    )
                )

    def setup(self, config_file: IO[Any]) -> None:
        self.data = json.load(config_file)

        desired_month = datetime.datetime.strptime(
            self.data["substitutions"]["MONTH"], "%B"
        )
        desired_year = int(self.data["substitutions"]["YEAR"])

        self.data["substitutions"]["MONTH_INT"] = desired_month.strftime("%m")
        self.data["substitutions"]["MONTH_LENGTH"] = str(
            monthrange(desired_year, desired_month.month)[1]
        )
        self.data["substitutions"]["NEXT_MONTH"] = (
            desired_month + relativedelta.relativedelta(months=1)
        ).strftime("%B")

    def print_substitutions(self) -> None:
        # TODO implement this
        raise NotImplementedError
