from abc import ABC, abstractmethod
from typing import Dict


class TextSubstitutor(ABC):
    @abstractmethod
    def get_substitutions(self) -> Dict[str, str]:
        pass
