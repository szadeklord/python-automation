from datetime import date, timedelta
from typing import Dict


class FailsafeDict(Dict[str, str]):
    def __missing__(self, key: str) -> str:
        return "{" + str(key) + "}"


def is_successful_http_code(response_code: int) -> bool:
    return (
        200 <= response_code < 300
    )  # Ensure our return code is a success code (any 200 number)


def prev_non_weekend_day(date: date) -> date:
    while date.weekday() > 4:  # Mon-Fri are 0-4
        date -= timedelta(days=1)
    return date
