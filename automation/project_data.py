from typing import Any, Dict


class ProjectData(object):
    def __init__(
        self, destination_url: str, config: Any, substitutions: Dict[str, str]
    ):
        self.destination_url = destination_url
        self.config = config
        self.substitutions = substitutions
