import getpass
import pickle
import time  # This really is used for a hack. We should find a more robust way to drive Selenium
from pathlib import Path
from typing import Any, Iterable, Union

# Selenium Packages
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait  # type: ignore
from seleniumrequests import Firefox  # type: ignore

from automation import utility


def keyboard_input_current_element(driver: Any, keys: Iterable[Any]) -> None:
    actions = ActionChains(driver)  # type: ignore

    for key in keys:
        actions.send_keys(key)  # type: ignore

    actions.perform()  # type: ignore


# TODO: Add in error handling with screenshot for user's knowledge of what is going on (maybe only if in headless mode)
def authenticate(config: Any) -> Union[None, Any]:
    # Get all relevant config values here for maintainability/readability
    try:
        selenium_headless = config["selenium"]["headless"]
        selenium_driver_path = config["selenium"]["driver_path"]
        sharepoint_endpoint = config["authentication"]["sharepoint"]["endpoint"]
        sharepoint_username = config["authentication"]["sharepoint"]["username"]
        sharepoint_redirect_timeout = config["authentication"]["sharepoint"][
            "redirect_timeout"
        ]
        duo_timeout = config["authentication"]["sharepoint"]["duo_timeout"]
        enable_cached_credentials = config["authentication"]["cache"]["enable"]
        cookie_pickle_file = Path(
            config["authentication"]["cache"]["pickle_cookie_path"]
        )
    except Exception as e:
        print("One or more configuration values for authentication are invalid.")
        print(e)
        exit()

    use_cached_credentials = enable_cached_credentials and cookie_pickle_file.exists()

    if not use_cached_credentials:
        sharepoint_password = getpass.getpass(
            "Please input password for account '{}': ".format(sharepoint_username)
        )

    selenium_options = Options()  # type: ignore
    selenium_options.headless = selenium_headless

    if selenium_driver_path:
        browser = Firefox(
            options=selenium_options, executable_path=selenium_driver_path
        )
    else:
        browser = Firefox(options=selenium_options)

    # First check to see if we have some cookies or if we need to authenticate again
    if use_cached_credentials:
        # Use cookies
        cookies = pickle.load(open(cookie_pickle_file, "rb"))

        # We must load the address first before loading our cookies otherwise we get an "invalid cookie domain" message. See: https://stackoverflow.com/questions/59877561/selenium-common-exceptions-invalidcookiedomainexception-message-invalid-cookie
        browser.get(sharepoint_endpoint)
        for cookie in cookies:
            browser.add_cookie(cookie)

        # Now that we have loaded cookies try the link again
        browser.get(sharepoint_endpoint)

        # If cookies fail we will still need to revert to the regular login process
        # TODO: Handle Cookies failing appropriately
    else:
        # First check to see what kind of response we get from an
        browser.get(sharepoint_endpoint)

        # Get the current URL after any redirects so that we can determine when we are fully on a new page
        current_url = browser.current_url

        time.sleep(
            2
        )  # Again this is a hack we will want a more robust way of driving selenium
        # Try this solution next: https://www.google.com/search?q=selenium+wait+for+page+to+fully+load&rlz=1C1GCEU_enUS968US968&oq=selenium+wait+for+page+to+fully+load&aqs=chrome.0.0i512l2j0i22i30l2.5054j0j7&sourceid=chrome&ie=UTF-8
        keyboard_input_current_element(browser, [sharepoint_username, Keys.RETURN])

        # Wait for the login to re-direct us.
        WebDriverWait(browser, sharepoint_redirect_timeout).until(
            EC.url_changes(current_url)  # type: ignore
        )
        current_url = browser.current_url

        time.sleep(
            2
        )  # Again this is a hack we will want a more robust way of driving selenium
        keyboard_input_current_element(browser, [sharepoint_password, Keys.RETURN])

        # Next we need to handle the TFA
        WebDriverWait(browser, duo_timeout).until(EC.url_changes(current_url))  # type: ignore

        time.sleep(
            2
        )  # Again this is a hack we will want a more robust way of driving selenium
        # Next we need to tell the prompt that we wish to stay logged in
        elem = browser.switch_to.active_element

        if str(elem.get_attribute("value")).lower() == "yes":
            actions = ActionChains(browser)  # type: ignore
            actions.send_keys(Keys.RETURN)  # type: ignore
            actions.perform()  # type: ignore

        pickle.dump(browser.get_cookies(), open(cookie_pickle_file, "wb"))
        browser.get(sharepoint_endpoint)

    response = browser.request("GET", sharepoint_endpoint)

    if utility.is_successful_http_code(response.status_code):
        return browser
    else:
        return None
