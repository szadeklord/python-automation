import datetime
import json
from pathlib import Path
from typing import Any, Dict, Union

from docx import Document  # type: ignore

from automation import project_data, utility
from automation.report_data import ReportData


def dict_replace_docx(doc: Document, dict: Dict[str, str]) -> None:
    def shuttle_text(shuttle: Any) -> str:
        t = ""
        for i in shuttle:
            t += i.text
        return t

    for key in dict:
        match_key = "{{{}}}".format(key)
        for p in doc.paragraphs:
            begin = 0
            for end in range(len(p.runs)):

                shuttle = p.runs[begin : end + 1]

                full_text = shuttle_text(shuttle)
                if match_key in full_text:
                    # print('Replace：', key, '->', data[key])
                    # print([i.text for i in shuttle])

                    # find the begin
                    index = full_text.index(match_key)
                    # print('full_text length', len(full_text), 'index:', index)
                    while index >= len(p.runs[begin].text):
                        index -= len(p.runs[begin].text)
                        begin += 1

                    shuttle = p.runs[begin : end + 1]

                    # do replace
                    # print('before replace', [i.text for i in shuttle])
                    if match_key in shuttle[0].text:
                        shuttle[0].text = shuttle[0].text.replace(match_key, dict[key])
                    else:
                        replace_begin_index = shuttle_text(shuttle).index(match_key)
                        replace_end_index = replace_begin_index + len(match_key)
                        replace_end_index_in_last_run = replace_end_index - len(
                            shuttle_text(shuttle[:-1])
                        )
                        shuttle[0].text = (
                            shuttle[0].text[:replace_begin_index] + dict[key]
                        )

                        # clear middle runs
                        for i in shuttle[1:-1]:
                            i.text = ""

                        # keep last run
                        shuttle[-1].text = shuttle[-1].text[
                            replace_end_index_in_last_run:
                        ]

                    # print('after replace', [i.text for i in shuttle])

                    # set begin to next
                    begin = end


def generate_reports(config: Any, browser: Any) -> Union[None, ReportData]:
    # Get all relevant config values here for maintainability/readability
    try:
        substitutions = config["substitutions"]
        dry_run = config["reports"]["dry_run"]
        overwrite_str = "true" if config["reports"]["overwrite"] else "false"
        projects = config["reports"]["projects"]
        temp_path = Path(config["reports"]["temp_path"])
        lead_days = int(config["reports"]["lead_days"])
        sharepoint_base_url = config["authentication"]["sharepoint"]["endpoint"]
    except Exception as e:
        print("One or more configuration values for reports are invalid.")
        print(e)
        exit()

    status = True
    reports_returned = ReportData(config)
    # Turn substitutions from a regular dict int  a failsafe dict so we can lazily replace without errors
    substitutions = utility.FailsafeDict(substitutions)

    for project in projects:
        project_substitutions = utility.FailsafeDict(substitutions)
        desired_deadline = datetime.date(
            int(substitutions["YEAR"]),
            datetime.datetime.strptime(substitutions["NEXT_MONTH"], "%B").month,
            project["msr_deadline"],
        )
        adjusted_deadline = utility.prev_non_weekend_day(desired_deadline)
        lead_date = adjusted_deadline.replace(day=adjusted_deadline.day - lead_days)

        project_substitutions["LEAD_MONTH"] = utility.prev_non_weekend_day(
            lead_date
        ).strftime("%B")
        project_substitutions["LEAD_DUE_DATE"] = utility.prev_non_weekend_day(
            lead_date
        ).strftime("%d")

        project_substitutions["PROJECT_MEMBERS"] = "/".join(
            [
                member_email.split(".")[0].capitalize()
                for member_email in project["members"]
            ]
        )
        project_substitutions["PROJECT_NAME"] = project["name"]
        project_substitutions.update(project["substitutions"])

        project_sharepoint_site_name = project["sharepoint_site_name"]
        project_sharepoint_type = project["sharepoint_typename"]
        template_sharepoint_relative_path = project["template_sharepoint_relative_path"]
        output_report_dir_relative_path = project["output_report_dir_relative_path"]
        template_file_name = template_sharepoint_relative_path.split("/")[-1]
        temp_file_name = template_file_name.format_map(substitutions)

        # Attempt to download the template file.
        file_url = "{}{}/{}/_api/Web/GetFileByServerRelativePath(decodedurl='/{}/{}/{}')/$value".format(
            sharepoint_base_url,
            project_sharepoint_type,
            project_sharepoint_site_name,
            project_sharepoint_type,
            project_sharepoint_site_name,
            template_sharepoint_relative_path,
        )
        template_file_download_response = browser.request("GET", file_url)

        with open(temp_path / temp_file_name, "wb") as out_file:
            out_file.write(template_file_download_response.content)

        template_file_download_success = utility.is_successful_http_code(
            template_file_download_response.status_code
        )

        if not template_file_download_success:
            print(
                f"Template file not found or accessible. File:{template_sharepoint_relative_path}"
            )
            status = False
            break

        # Now that the template file has been dragged down from Sharepoint make any text replacements before re-uploading to Sharepoint
        # Open the template document
        document = Document(temp_path / temp_file_name)

        dict_replace_docx(document, project_substitutions)

        document.save(temp_path / temp_file_name)

        destination_url = ""

        if not dry_run:
            # Re-upload to sharepoint
            with open(temp_path / temp_file_name, "rb") as in_file:
                payload = in_file.read()

            digest_headers = {"Accept": "application/json;odata=verbose"}
            request_digest_url = "{}{}/{}/_api/contextinfo".format(
                sharepoint_base_url,
                project_sharepoint_type,
                project_sharepoint_site_name,
            )
            response = browser.request(
                "POST", request_digest_url, headers=digest_headers
            )
            digest_success = utility.is_successful_http_code(response.status_code)

            if not digest_success:
                print("Unable to obtain file upload request digest.")
                status = False
                break

            digest = json.loads(response.content)["d"]["GetContextWebInformation"][
                "FormDigestValue"
            ]
            upload_headers = {
                "Accept": "application/json;odata=verbose",
                "x-requestdigest": digest,
            }
            upload_url = "{}{}/{}/_api/web/getfolderbyserverrelativeurl('{}')/files/add(overwrite={}, url='{}')".format(
                sharepoint_base_url,
                project_sharepoint_type,
                project_sharepoint_site_name,
                output_report_dir_relative_path,
                overwrite_str,
                temp_file_name,
            )

            response = browser.request(
                "POST", upload_url, headers=upload_headers, data=payload
            )
            upload_success = utility.is_successful_http_code(response.status_code)

            if not upload_success:
                print(response.content)
                status = False
                break

            destination_url = json.loads(response.content)["d"]["LinkingUrl"]

        # Create project object that will be passed back to any subsequent steps
        project_return = project_data.ProjectData(
            destination_url, project, project_substitutions
        )
        reports_returned.projects.append(project_return)

    if status:
        return reports_returned
    else:
        return None
