# python-automation

There are a number job requirements in day to day life that involve re-occurring manual task completion. This library is here to provide a base set of capabilities to automate these tasks away as much as possible.

# Optional Pre-Requisites for Web Automation 
- Firefox
- Firefox Selenium Webdriver (Geckodriver)

# Installation Instructions for Optional Pre-Requisites

1. Install Firefox if you have not already.
    - https://www.mozilla.org/en-US/firefox/download

2. Download the latest copy of Mozilla's Geckodriver
    - https://github.com/mozilla/geckodriver
    - For example at the time of writing (03/07/2022) it was: https://github.com/mozilla/geckodriver/releases/download/v0.30.0/geckodriver-v0.30.0-win64.zip

3. Extract the binary to a relevant location.
    - This can either be in the repo directory, the Firefox installation directory, or any location of your choosing.

# Developing/Contributing

## Build process

1. Navigate to the top level directory
2. Ensure `pip` is up to date:
    - `python -m pip install --upgrade pip`
3. Ensure python build tools are up to date:
    - `python -m pip install --upgrade build`
4. Build the python package into the `./dist`
    - `python -m build`

## Deploy process

1. Ensure deploy package is up to date
    - `python -m pip install --upgrade twine`
2. Package up distribution packages 
    - `python -m twine upload --repository <REPO_LINK> dist/*`
        - Where: `<REPO_LINK>` is the path to your desired python package repository.

## Installing package from PyPi repository

1. Install package with pip
    - `python -m pip install --index-url <INDEX_URL> --no-deps python-automation`
        - Where: `<INDEX_URL>` is the path to your desired python package repository.